export let renderListKinh = dataGlasses => {
    let content = "";
    dataGlasses.forEach(glass => {
        content += `
        <a class="col-4">
            <img onclick="ShowKinh('${glass.id}')" src="${glass.src}" alt="" />
        </a>`;
    });
    document.getElementById("vglassesList").innerHTML = content;
    document.getElementById("glassesInfo").style.display = "none";
}

export let TimKiemVTKinh = (ma, dstk) => {
    for(var i = 0; i<dstk.length; i++)
    {
        let vt = dstk[i];
        if(vt.id == ma){
            console.log('dstk[i]: ', dstk[i]);
            return i;
        }
    }
    return -1;
}

export let ShowGlassFace = (glass) => {
    let content = `
    <div ">
        <img src="${glass.virtualImg}" alt="avatar" id="facekinh"/>
    </div>`;
    document.getElementById("avatar").innerHTML = content;
}

export let ShowThongTinKinh = (glass) => {
    let content = `
    <div>
        <p>${glass.name} - ${glass.brand} (${glass.color})</p>
        <p id="gia">$${glass.price}</p>
        <p>${glass.description}</p>
    </div>`;
    document.getElementById("glassesInfo").innerHTML = content;
    document.getElementById("glassesInfo").style.display = "block";
}